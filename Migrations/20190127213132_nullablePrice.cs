﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArtManager.Migrations
{
    public partial class nullablePrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Arts",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Arts",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}

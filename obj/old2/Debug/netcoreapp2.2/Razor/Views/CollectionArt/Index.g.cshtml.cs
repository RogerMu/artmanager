#pragma checksum "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "47e7fdb645f29b71e7e22b6ebd1da9fc97e5235f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CollectionArt_Index), @"mvc.1.0.view", @"/Views/CollectionArt/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/CollectionArt/Index.cshtml", typeof(AspNetCore.Views_CollectionArt_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/Users/Roger/Projects/ArtManager/ArtManager/Views/_ViewImports.cshtml"
using ArtManager;

#line default
#line hidden
#line 2 "/Users/Roger/Projects/ArtManager/ArtManager/Views/_ViewImports.cshtml"
using ArtManager.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"47e7fdb645f29b71e7e22b6ebd1da9fc97e5235f", @"/Views/CollectionArt/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"da29fc5a4224283a970ed65a859e7a38f4aefbd4", @"/Views/_ViewImports.cshtml")]
    public class Views_CollectionArt_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Models.CollectionArt>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(42, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(85, 29, true);
            WriteLiteral("\r\n<h2>Index</h2>\r\n\r\n<p>\r\n    ");
            EndContext();
            BeginContext(114, 37, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "47e7fdb645f29b71e7e22b6ebd1da9fc97e5235f3757", async() => {
                BeginContext(137, 10, true);
                WriteLiteral("Create New");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(151, 92, true);
            WriteLiteral("\r\n</p>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(244, 39, false);
#line 16 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Art));

#line default
#line hidden
            EndContext();
            BeginContext(283, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(339, 46, false);
#line 19 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Collection));

#line default
#line hidden
            EndContext();
            BeginContext(385, 86, true);
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
            EndContext();
#line 25 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
            BeginContext(503, 48, true);
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(552, 44, false);
#line 28 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Art.ArtId));

#line default
#line hidden
            EndContext();
            BeginContext(596, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(652, 58, false);
#line 31 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Collection.CollectionId));

#line default
#line hidden
            EndContext();
            BeginContext(710, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(766, 64, false);
#line 34 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.ActionLink("Edit", "Edit", new { item.Art.CollectionArts} ));

#line default
#line hidden
            EndContext();
            BeginContext(830, 20, true);
            WriteLiteral(" |\r\n                ");
            EndContext();
            BeginContext(851, 54, false);
#line 35 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.ActionLink("Details", "Details", new { id=item }));

#line default
#line hidden
            EndContext();
            BeginContext(905, 20, true);
            WriteLiteral(" |\r\n                ");
            EndContext();
            BeginContext(926, 69, false);
#line 36 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
           Write(Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ }));

#line default
#line hidden
            EndContext();
            BeginContext(995, 36, true);
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
            EndContext();
#line 39 "/Users/Roger/Projects/ArtManager/ArtManager/Views/CollectionArt/Index.cshtml"
}

#line default
#line hidden
            BeginContext(1034, 24, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Models.CollectionArt>> Html { get; private set; }
    }
}
#pragma warning restore 1591

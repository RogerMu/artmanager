﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Models
{
    public class Art
    {

        public int ArtId { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string PriceText { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string ImageWidths { get; set; }
        public string ImageOriginal { get; set; }
        public bool Hidden { get; set; }

        public DateTime Created { get; set; }
      
        //[DataType(DataType.DateTime)]
        public DateTime Updated { get; set; }


        public IEnumerable<CollectionArt> CollectionArts { get; set; }
    }
}

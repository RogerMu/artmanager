using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Models;

namespace Models
{
    public class ArtContext: DbContext
    {
        public ArtContext(DbContextOptions<ArtContext> options)
            :base(options)
        {
        }

        public DbSet<Collection> Collections { get; set; }
        public DbSet<Art> Arts { get; set; }
        public DbSet<CollectionArt> CollectionArts { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //=> optionsBuilder.UseSqlite();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CollectionArt>()
                        .HasKey(ca => new { ca.CollectionId, ca.ArtId });

            modelBuilder.Entity<CollectionArt>()
                        .HasOne(ca => ca.Collection)
                        .WithMany(c => c.CollectionArts)
                        .HasForeignKey(ca => ca.CollectionId);

            modelBuilder.Entity<CollectionArt>()
                        .HasOne(ca => ca.Art)
                        .WithMany(c => c.CollectionArts)
                        .HasForeignKey(ca => ca.ArtId);

            modelBuilder.Entity<Art>()
                        .Property<DateTime>("Created")
                        .ValueGeneratedNever();//avoid to set new time on Edit
                        //.ValueGeneratedOnAdd();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //=> optionsBuilder.UseSqlite();

        //public DbSet<Models.CollectionArt> CollectionArt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ViewModels;

namespace Models
{
    public class Collection
    {
        
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //[DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{yyyy MM dd HH:mm:ss}" )]
        public DateTime Created { get; set; }
        //[DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{yyyy MM dd HH:mm:ss}")]
        public DateTime Updated { get; set; }
        public bool Hidden { get; set; }

        //public ICollection<CollectionArt> Arts { get; set; } = new List<CollectionArt>();
        public IEnumerable<CollectionArt> CollectionArts { get; set; }

        //public IEnumerable<CollectionArt> CollectionArt { get; internal set; }
    }
}

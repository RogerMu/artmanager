﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;


namespace Models
{
    public class CollectionArt
    {
        public int ArtId { get; set; }
        public Art Art { get; set; }

        public int CollectionId { get; set; }
        public Collection Collection { get; set; }

    }
}

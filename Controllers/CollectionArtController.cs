using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Models;

namespace ArtManager.Controllers
{
    public class CollectionArtController : Controller
    {
        private readonly ArtContext _context;

        public CollectionArtController(ArtContext context)
        {
            _context = context;
        }

        // GET: CollectionArt
        public async Task<IActionResult> Index()
        {
            var artContext = _context.CollectionArts
                .Include(c => c.Art)
                .Include(c => c.Collection)
                .OrderBy(o => o.CollectionId)
                .ThenBy(o => o.ArtId);
            return View(await artContext.ToListAsync());
        }

        // GET: CollectionArt/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionArt = await _context.CollectionArts
                .Include(c => c.Collection)
                .OrderBy(o => o.CollectionId)
                .Include(c => c.Art)
                .OrderBy(o => o.ArtId)
                .FirstOrDefaultAsync(m => m.CollectionId == id);
            if (collectionArt == null)
            {
                return NotFound();
            }

            return View(collectionArt);
        }

        // GET: CollectionArt/Create
        public IActionResult Create()
        {
            ViewData["ArtId"] = new SelectList(_context.Arts, "ArtId", "ArtId");
            ViewData["CollectionId"] = new SelectList(_context.Collections, "CollectionId", "CollectionId");
            return View();
        }

        // POST: CollectionArt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ArtId,CollectionId")] CollectionArt collectionArt)
        {
            if (ModelState.IsValid)
            {
                _context.Add(collectionArt);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ArtId"] = new SelectList(_context.Arts, "ArtId", "ArtId", collectionArt.ArtId);
            ViewData["CollectionId"] = new SelectList(_context.Collections, "CollectionId", "CollectionId", collectionArt.CollectionId);
            return View(collectionArt);
        }

        // GET: CollectionArt/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionArt = await _context.CollectionArts.FindAsync(id);
            if (collectionArt == null)
            {
                return NotFound();
            }
            ViewData["ArtId"] = new SelectList(_context.Arts, "ArtId", "ArtId", collectionArt.ArtId);
            ViewData["CollectionId"] = new SelectList(_context.Collections, "CollectionId", "CollectionId", collectionArt.CollectionId);
            return View(collectionArt);
        }

        // POST: CollectionArt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ArtId,CollectionId")] CollectionArt collectionArt)
        {
            if (id != collectionArt.CollectionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(collectionArt);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionArtExists(collectionArt.CollectionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(collectionArt);
        }

        // GET: CollectionArt/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionArt = await _context.CollectionArts
                .Include(c => c.Art)
                .Include(c => c.Collection)
                .FirstOrDefaultAsync(m => m.CollectionId == id);
            if (collectionArt == null)
            {
                return NotFound();
            }

            return View(collectionArt);
        }

        // POST: CollectionArt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var collectionArt = await _context.CollectionArts.FindAsync(id);
            _context.CollectionArts.Remove(collectionArt);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollectionArtExists(int id)
        {
            return _context.CollectionArts.Any(e => e.CollectionId == id);
        }
    }
}

using System;
//using System.Collection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
//using System.Data.Entity;
//using System.Data.Entity.EntityState.Modified;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using Microsoft.EntityFrameworkCore;
using Models;
using ViewModels;
using ArtManager.Models;
using static System.Linq.Enumerable;
using System.Collections;

namespace ArtManager.Controllers
{

    public class CollectionsController : Controller
    {

        CollectionEditVM collectionEditVM = new CollectionEditVM();
        CollectionEditArtsVM collectionEditArtsVM = new CollectionEditArtsVM();

        private readonly ArtContext _context;

        public CollectionsController(ArtContext context)
        {
            _context = context;
        }

      

        // GET: Collections
        public async Task<IActionResult> Index()
        {
            return View(await _context.Collections.ToListAsync());
        }




        ////------------------------------
        //Get 
        public async Task<IActionResult> Art(int? id)
        {
            CollectionAddArtVM collectionAddArtVM = new CollectionAddArtVM();

            if (id == null)
            {
                return NotFound();
            }

            //hämtar alla bilder från db till view
            List<Art> arts = await _context.Arts.ToListAsync();
          

            collectionEditArtsVM.AllArts = arts;


            var collectionsArt = _context.Arts.Where(s => s.CollectionArts.Any(i => i.CollectionId == id)).ToList();
            collectionEditArtsVM.CollectionsArt = collectionsArt;//(Models.Art)collectionsArt;


            //var collectionsArtList = collectionsArt.ToList();

            //exclude current art in current collection and keep the rest of art to display
            IEnumerable<Art> allArtExceptCollections = arts.Where(x => !collectionsArt.Select(c => c.ArtId).Contains(x.ArtId));

            //remove preselected checkboxes
            foreach (var a in allArtExceptCollections)
            {
                a.Hidden = false;
            }

            //bring current collection
            var collection = await _context.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }

            if (allArtExceptCollections != null)
            {
                collectionEditArtsVM.AllArtExceptCurrent = allArtExceptCollections.ToList();
            }

            collectionEditArtsVM.AllArtExceptCurrent = allArtExceptCollections.ToList();

            collectionEditArtsVM.Collection = collection;

            return View(collectionEditArtsVM);
            //return View(arts);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Art(int id, CollectionEditArtsVM _collectionEditArtsVM)
        {

            if (ModelState.IsValid && _collectionEditArtsVM != null)
            {

                //linq get all Art that belongs actual collection from db
                var collectionsArt = _context.Arts.Where(s => s.CollectionArts.Any(i => i.CollectionId == id)).ToList();

                //adding choosed Art to choosedArt list 
                List<Art> choosedArt = new List<Art>();
                //for (var i = 0; i < _collectionEditArtsVM.AllArts.Count(); i++)
                for (var i = 0; i < _collectionEditArtsVM.AllArtExceptCurrent.Count(); i++)
                {
                    //not the Art already existed in actual collection
                    if (_collectionEditArtsVM.AllArtExceptCurrent[i].Hidden == true)
                    {
                        if (! collectionsArt.Any(x => x.ArtId == _collectionEditArtsVM.AllArtExceptCurrent[i].ArtId))
                        {
                            choosedArt.Add(_collectionEditArtsVM.AllArtExceptCurrent[i]);
                        }
                    }
                }

                //coupling Art and actual collection with its id to one CollectionArt-object.
                var c = choosedArt.Count();
                var colArt = new List<CollectionArt>();

                for (var i = 0; i < c; i++)//foreach (Art item in choosedArt)
                {
                    colArt.Add(new CollectionArt { ArtId = choosedArt[i].ArtId, CollectionId = id });
                }

                //transfer Art to CollectionArt table in db 
                foreach(CollectionArt ca in colArt)
                {
                    _context.CollectionArts.Add(ca);
                }

                _context.SaveChangesAsync();

            }

            return View(_collectionEditArtsVM);
        }


        // GET: Collections/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collections
                .FirstOrDefaultAsync(m => m.CollectionId == id);
            if (collection == null)
            {
                return NotFound();
            }

            IQueryable<CollectionArt> artInCollection = _context.CollectionArts.Where(c=>c.CollectionId == id);

            ViewData["arts"] = artInCollection.ToList();

            return View(collection);
        }

        // GET: Collections/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Collections/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CollectionId,Name,Description,Created,Updated,Hidden,CollectionArt,ArtId")] Collection collection)
        {
            if (ModelState.IsValid)
            {
                collection.Created = DateTime.UtcNow;

                _context.Add(collection);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(collection);
        }


        // GET: Collections/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }


            //get all Art
            List<Art> art = _context.Arts.AsNoTracking<Art>().ToList();
            collectionEditVM.AllArts = art;

            //bring Art that belongs actual collection 
            List<Art> collectionsArt = _context.Arts.Where(s => s.CollectionArts.Any(i => i.CollectionId == id)).ToList();
            collectionEditVM.CollectionsArt = collectionsArt;

            collectionEditVM.Collection = collection;//populate the usual textboxes etc in Collection Edit View by VM


            //ViewData["ArtId"] = new SelectList((System.Collections.IEnumerable)collectionsArt, "ArtId", "Name");

            return View(collectionEditVM);
        }


        // POST: Collections/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CollectionEditVM _collection)
        {

            if (ModelState.IsValid)
            {

                _collection.Collection.Updated = DateTime.UtcNow;

                Collection collection = new Collection();

                collection = _collection.Collection;
                try
                {

                    _context.Update(collection);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionExists(_collection.Collection.CollectionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }


            return View(_collection);
        }


        //public async Task <IActionResult> RemoveArt2(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var collection = await _context.Collections
        //        .FirstOrDefaultAsync(m => m.CollectionId == id);
        //    if (collection == null)
        //    {
        //        return NotFound();
        //    }

        //    //get art that belonging current collection 
        //    List<Art> collectionsArt = _context.Arts.Where(s => s.CollectionArts.Any(i => i.CollectionId == id)).ToList();
        //    collectionEditVM.CollectionsArt = collectionsArt;
        //    collectionEditVM.Collection = collection;


        //    return View(collectionEditVM);
        //}

        //separate Post and Get not needed
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task <IActionResult> RemoveArt(int id, int artId)
        {

            //var collectionArt = await _context.CollectionArts.FirstOrDefaultAsync(c => c.CollectionId == id);
            //var collectionArt = _context.CollectionArts.Find(Collection.CollectionId == id) Model.Art.ArtId == artId;
            CollectionArt _collectionArtToRemove = new CollectionArt { CollectionId = id, ArtId = artId };
          
            _context.CollectionArts.Remove(_collectionArtToRemove);

            //_context.Collections

            await _context.SaveChangesAsync();


            var collection = await _context.Collections
                .FirstOrDefaultAsync(m => m.CollectionId == id);
            if (collection == null)
            {
                return NotFound();
            }

            //get art that belonging current collection 
            List<Art> collectionsArt = _context.Arts.Where(s => s.CollectionArts.Any(i => i.CollectionId == id)).ToList();
            collectionEditVM.CollectionsArt = collectionsArt;
            collectionEditVM.Collection = collection; //populate collection details


            return View("Edit", collectionEditVM);
        }

        // GET: Collections/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collections
                .FirstOrDefaultAsync(m => m.CollectionId == id);
            if (collection == null)
            {
                return NotFound();
            }

            return View (collection);
        }

        // POST: Collections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var collection = await _context.Collections.FindAsync(id);
            _context.Collections.Remove(collection);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollectionExists(int id)
        {
            return _context.Collections.Any(e => e.CollectionId == id);
        }
    }
}

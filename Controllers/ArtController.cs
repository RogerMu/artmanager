using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Helpers;
using Models;

namespace ArtManager.Controllers
{
    public class ArtController : Controller
    {
        private readonly ArtContext _context;
        private IHostingEnvironment _env; //håller reda på sökvägar
        private readonly IDeleteArtImages _deleteArtImages;

        public ArtController(ArtContext context, IHostingEnvironment env, IDeleteArtImages deleteArtImages)
        {
            _context = context;
            _env = env;
            _deleteArtImages = deleteArtImages;
        }

        // GET: Art
        public async Task<IActionResult> Index()
        {
            return View(await _context.Arts.ToListAsync());
        }

        // GET: Art/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var art = await _context.Arts
                .FirstOrDefaultAsync(m => m.ArtId == id);
            if (art == null)
            {
                return NotFound();
            }


            //Loading/show current picture
            string imagePathAndName = art.ImagePath;
            imagePathAndName += "/" + art.ImageName;
            ViewBag.imagePathAndName = imagePathAndName;


            return View(art);
        }

        // GET: Art/Create
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ArtId,Name,Artist,Description,Price,PriceText,ImageName,ImagePath,ImageWidths,ImageOriginal,Hidden")] Art art, IFormFile ImgFile)
        {

            if (ImgFile == null && ImgFile.Length > 0)
            {
                //throw new ServiceException(ServiceErrorCodes.InvalidPhoto, "There is no photo.");
                return Content("File not found");
            }
            //Get application directory
            string webRoot = _env.WebRootPath;

            if (ModelState.IsValid)
            {
                var loadedOriginalFileName = "";
                var loadedTempFilePath = "";
                var loadedTempFilePathAndFileName = "";

                loadedOriginalFileName = "aid" + art.ArtId + "-" + Guid.NewGuid();

                if (!Directory.Exists(Path.Combine(webRoot, "tempImages")))
                {
                    Directory.CreateDirectory(Path.Combine(webRoot, "tempImages"));
                }
                loadedTempFilePath = Path.Combine(webRoot, "tempImages");

                loadedTempFilePathAndFileName = Path.Combine(loadedTempFilePath, loadedOriginalFileName);

                //save file from view to disc
                using (var fileStream = new FileStream(loadedTempFilePathAndFileName, FileMode.Create))
                {
                    await ImgFile.CopyToAsync(fileStream);
                }

                //date folder must create one time to all article picture if the date change in meantime
                var pictureFolderDirectory = ImagePathBuilderClass.ImagePathBuilder(webRoot);

                //Retrive relative picture folder directory
                int startPosToSelect = pictureFolderDirectory.IndexOf("wwwroot/Upload", System.StringComparison.CurrentCultureIgnoreCase);
                string relativePictureFolderDirectory = pictureFolderDirectory.Substring(startPosToSelect + 8); //cut after "wwwroot/"
                art.ImagePath = relativePictureFolderDirectory;

                art.ImageWidths = DefineImageSizesClass.DefineImageSizes(ImgFile, loadedTempFilePathAndFileName, pictureFolderDirectory, loadedOriginalFileName);


                art.ImageName = loadedOriginalFileName; //originalFilename;

                art.Created = DateTime.UtcNow;
                art.Updated = DateTime.UtcNow;
                //if (art.Created == DateTime.MinValue) //check if already created
                //{
                //    art.Created = DateTime.Now;
                //}

                DeleteOriginalTempFileClass.DeleteOriginalTempFile(loadedTempFilePathAndFileName); //loadedFilePathAndFileName);

                _context.Add(art);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(art);
        }

        // GET: Art/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var art = await _context.Arts.FindAsync(id);


            if (art == null)
            {
                return NotFound();
            }
            return View(art); 
        }

        // POST: Art/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ArtId,Name,Artist,Description,Price,PriceText,ImageName,ImagePath,ImageWidths,ImageOriginal,Hidden")] Art art, IFormFile ImgFile)
        {
            if (id != art.ArtId)
            {
                return NotFound();
            }

            //Loading/show current picture
            string imagePathAndName = art.ImagePath;
            imagePathAndName += "/" + art.ImageName;
            ViewBag.imagePathAndName = imagePathAndName;

            //Get application directory
            string webRoot = _env.WebRootPath;
            string contentRootPath = _env.ContentRootPath;

            if (ModelState.IsValid)
            {
                if (ImgFile != null && ImgFile.Length != 0)
                {

                    var loadedOriginalFileName = "";
                    var loadedTempFilePath = "";
                    var loadedTempFilePathAndFileName = "";

                    loadedOriginalFileName = "aid" + art.ArtId + "-" + Guid.NewGuid();

                    if (!Directory.Exists(Path.Combine(webRoot, "tempImages")))
                    {
                        Directory.CreateDirectory(Path.Combine(webRoot, "tempImages"));
                    }
                    loadedTempFilePath = Path.Combine(webRoot, "tempImages");
                    loadedTempFilePathAndFileName = Path.Combine(loadedTempFilePath, loadedOriginalFileName);

                    //save file from view to disc
                    using (var fileStream = new FileStream(loadedTempFilePathAndFileName, FileMode.Create))
                    {
                        await ImgFile.CopyToAsync(fileStream);
                    }

                    //date folder must create one time to all article picture if the date change in meantime
                    var pictureFolderDirectory = ImagePathBuilderClass.ImagePathBuilder(webRoot);
                    //Retrive relative picture folder directory
                    int startPosToSelect = pictureFolderDirectory.IndexOf("wwwroot/Upload", System.StringComparison.CurrentCultureIgnoreCase);
                    string relativePictureFolderDirectory = pictureFolderDirectory.Substring(startPosToSelect + 8); //cut after wwwroot/
                    //save old path to delete old pics
                    var oldFilePath = art.ImagePath;
                    //new fresh path
                    art.ImagePath = relativePictureFolderDirectory;

                    //create new pictures
                    art.ImageWidths = DefineImageSizesClass.DefineImageSizes(ImgFile, loadedTempFilePathAndFileName, pictureFolderDirectory, loadedOriginalFileName);


                    var oldFileNameToDelete = art.ImageName;//get old pic name

                    art.ImageName = loadedOriginalFileName; //Filename from update picture download;

                    _deleteArtImages.DeleteImages(oldFilePath, oldFileNameToDelete);

                    DeleteOriginalTempFileClass.DeleteOriginalTempFile(loadedTempFilePathAndFileName);
                }

                art.Updated = DateTime.UtcNow;
                art.Created = await _context.Arts.Where( a => a.ArtId == id).Select(a=>a.Created).FirstOrDefaultAsync() ;

                try
                {

                    _context.Update(art);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtExists(art.ArtId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(art);
        }

        // GET: Art/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var art = await _context.Arts
                .FirstOrDefaultAsync(m => m.ArtId == id);
            if (art == null)
            {
                return NotFound();
            }

            return View(art);
        }

        // POST: Art/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var art = await _context.Arts.FindAsync(id);
            _context.Arts.Remove(art);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArtExists(int id)
        {
            return _context.Arts.Any(e => e.ArtId == id);
        }
    }
}

﻿using ArtManager.Controllers;
using Models;

namespace Helpers
{
    public interface IDeleteArtImages
    {
        //void DeleteArtImages();
        void DeleteImages(string path, string name);
    }
}
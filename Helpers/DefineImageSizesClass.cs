﻿using System;
using System.Drawing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using ArtManager.Controllers;
using Models;

namespace Helpers
{
    public class DefineImageSizesClass
    {
       
        public static string DefineImageSizes(IFormFile _imgFile, string loadedFilePathAndFileName, string pictureFolderDirectory, string loadedFileName)//, Image _imgFileBitmap)
        {
            System.IO.Stream imageStream = _imgFile.OpenReadStream();
            Image resizeImage = Image.FromStream(imageStream);
            var imageFileBitmapSize = resizeImage.Size;
            string imageWidths = ""; //store all sizes, separated with comma ","

            //Squarify the original size image
            // determain witch side is larger to use
            Int32 largestSideOfImage = resizeImage.Width > resizeImage.Height ? resizeImage.Width : resizeImage.Height;
            MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(resizeImage.Width, resizeImage.Height, imageFileBitmapSize,
            resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
            imageWidths += largestSideOfImage + ",";



            if (resizeImage.Width >= 2048)
            {
                MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(2048, 2048, imageFileBitmapSize, 
                resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
                imageWidths += "2048,";
            }

            //if (resizeImage.Width >= 1024)
            //{
            //    MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(1024, 1024, imageFileBitmapSize, resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
            //    imageWidths += "1024,";
            //}

            if (resizeImage.Width >= 512)   //used in edit mode
            {
                MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(512, 512, imageFileBitmapSize, resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
                imageWidths += "512,";
            }

            if (resizeImage.Width >= 256) //to use as thumbnail
            {
                MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(256, 256, imageFileBitmapSize, 
                resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
                imageWidths += "256,";
            }

            //if (resizeImage.Width >= 128)
            //{
            //    MakeImageSquareFillBlancsAndSaveClass.MakeImageSquareFillBlancsAndSave(128, 128, imageFileBitmapSize, resizeImage, loadedFilePathAndFileName, pictureFolderDirectory, loadedFileName);
            //    imageWidths += "128,";
            //}

            resizeImage.Dispose();
            imageStream.Dispose();

            return imageWidths;

           
        }
    }
}

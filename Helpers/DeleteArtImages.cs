﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using ArtManager.Controllers;
using Models;

namespace Helpers
{
   

    public class DeleteArtImages : IDeleteArtImages 
    {

        private readonly IHostingEnvironment _hostingEnv;

        public DeleteArtImages(IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv ?? throw new ArgumentNullException(nameof(hostingEnv));
        }

        public void DeleteImages(string path, string partialFileName)
        {
            string webRoot = _hostingEnv.WebRootPath;//wwwroot

            if (path == null || path == "") 
            { 
                System.Diagnostics.Debug.WriteLine("hittar inte sökvägen");
                return;
            };

            if (Directory.Exists(Path.Combine(webRoot, path +"/")))
            {
                var _fullPath = Path.Combine(webRoot, path + "/");

                string[] fileList = Directory.GetFiles(_fullPath, "*" + partialFileName + "*");
                foreach (string file in fileList)
                {
                    //todo: Make better dialog to user
                    System.Diagnostics.Debug.WriteLine(file + " kommer att raderas");
                    System.IO.File.Delete(file);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Hittar inte platsen där bilderna finns");


            }


        }
    }
}

﻿using System;
using System.IO;
using ArtManager.Controllers;
using Models;

namespace Helpers
{
    public class DeleteOriginalTempFileClass
    {
        public static void DeleteOriginalTempFile( string _loadedFilePathAndFileName)
        {
            
            if (System.IO.File.Exists(_loadedFilePathAndFileName))
            {
                System.IO.File.Delete(_loadedFilePathAndFileName);
            }
            else
            {
                System.Console.WriteLine("Path doesnt exist");
            }


            //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(loadedTempFilePath);

            //foreach (FileInfo file in di.GetFiles())
            //{
            //    file.Delete();
            //}
            //foreach (DirectoryInfo dir in di.GetDirectories())
            //{
            //    dir.Delete(true);
            //}

        }
    }
}

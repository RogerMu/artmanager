﻿using System;
using System.Drawing;
using System.IO;
using ArtManager.Controllers;
using Models;

namespace Helpers
{
    public class MakeImageSquareFillBlancsAndSaveClass
    {
       
        public static void MakeImageSquareFillBlancsAndSave(int canvasWidth, int canvasHeight, Size imgFileBitmapSize, Image imgFileBitmap, string loadedFilePathAndFileName, string pictureFolderDirectory, string loadedFileName)
        {
            var image = new Bitmap(loadedFilePathAndFileName);


            //Define new picture size
            System.Drawing.Image newPicSize = new System.Drawing.Bitmap(canvasWidth, canvasHeight);
            System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(newPicSize);

            graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            // Figure out the ratio
            double ratioX = (double)canvasWidth / (double)imgFileBitmap.Width;
            double ratioY = (double)canvasHeight / (double)imgFileBitmap.Height;
            // use whichever multiplier is smaller
            double ratio = ratioX < ratioY ? ratioX : ratioY;

            // now we can get the new height and width
            int newHeight = Convert.ToInt32(imgFileBitmap.Height * ratio);
            int newWidth = Convert.ToInt32(imgFileBitmap.Width * ratio);

            // Now calculate the X,Y position of the upper-left corner 
            // (one of these will always be zero)
            int posX = Convert.ToInt32((canvasWidth - (imgFileBitmap.Width * ratio)) / 2);
            int posY = Convert.ToInt32((canvasHeight - (imgFileBitmap.Height * ratio)) / 2);

            graphic.Clear(System.Drawing.Color.White); // white background
            graphic.DrawImage(image, posX, posY, newWidth, newHeight);//draw image atop the white background

            System.Drawing.Imaging.ImageCodecInfo[] info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            System.Drawing.Imaging.EncoderParameters encoderParameters;
            encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
            encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            using (var fileStream = new FileStream(pictureFolderDirectory + "/" + loadedFileName + "-" + canvasWidth + ".jpg", FileMode.Create))
            {
                newPicSize.Save(fileStream, info[1], encoderParameters);
            }

            image.Dispose();

        }
    }
}

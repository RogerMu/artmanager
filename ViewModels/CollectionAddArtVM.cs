﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

using Models;
using ViewModels;

namespace ViewModels
{
    public class CollectionAddArtVM
    {
        public Collection Collection { get; set; }
        public Art SelectedArt { get; set; }
        public Art AllArts { get; set; }
        public Art CollectionsArt { get; set; } //hämtar Art som tillhör Collection till view

        public List<bool> CheckedArt { get; set; }



    }
}

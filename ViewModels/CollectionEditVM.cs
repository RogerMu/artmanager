﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using ViewModels;

namespace ViewModels
{
    public class CollectionEditVM
    {
        [Display(Name = "Select art")]
        public string SelectedArt { get; set; }
        public List<Art> AllArts { get; set; }
        public List<Art> CollectionsArt { get; set; } //hämtar aktuell Art till view
        public Collection Collection { get; set; }

        //public int CollectionId { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public DateTime Created { get; set; }
        //public DateTime Updated { get; set; }
        //public bool Hidden { get; set; }

    }

}

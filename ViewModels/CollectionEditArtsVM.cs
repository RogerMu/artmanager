﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using ViewModels;
//using Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;

namespace ViewModels
{

    public class CollectionEditArtsVM
    {
        public Collection Collection { get; set; }
        public List<Art> SelectedArt { get; set; }
        public List<Art> AllArts { get; set; }
        public List<Art> CollectionsArt { get; set; } //hämtar Art som tillhör Collection till view
        public List<Art> AllArtExceptCurrent { get; set; }


        public List<bool> CheckedArt { get; set; }

    }
}
